$(document).ready(function () {
    $('#btnShowAllId').hide()

    filterData()
    getData()

});


// Función para buscar filtrar datos
function filterData(){

    $(document).on('click', '#btnId', function(e){
        e.preventDefault()
    
        let $search = $('#searchId').val()
        
        if(!$search || $search === ' '){
            Swal.fire({
                icon: 'error',
                title: 'No has ingresado un valor',
                text: 'Intenta nuevamente!',
              
              })
        }else{
             $.ajax({
                url:`http://kmmoto.test/${$search}`,
                success: function(data){
                    $('#tbodyId').html('')
                   let element = JSON.parse(data)
                    $('#tbodyId').append(`
        
                    <tr>
    
                    <th scope="row">${element.id}</th>
                    <td>${element.product_key}</td>
                    <td>${element.notes}</td>
                    <td>${element.qty}</td>
                    <td>${element.cost}</td>
                    <td>${element.price}</td>
                    
                   </tr>
        
                    `)
    
                    $('#btnShowAllId').show()
                },
                error: function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'No Se han encontrado productos con el valor ingresado',
                        text: 'Intenta nuevamente!',
                      
                      })
                   
                }
            })
        }
        
    })

}

// Obtener todo los registros
function getData(){

    $(document).on('click','#btnShowAllId', function(){
    
        $('#msmErroId').html('')    
        $('#linksId').hide()
        $('#searchId').val('')
        
        $.ajax({
            url:'http://kmmoto.test/index',
            success: function(data){
               let element = JSON.parse(data)
    
              $('#tbodyId').html('')
                element.forEach(el => {
                    $('#tbodyId').append(`
                        <tr>
                        <th scope="row">${el.id}</th>
                        <td>${el.product_key}</td>
                        <td>${el.notes}</td>
                        <td>${el.qty}</td>
                        <td>${el.cost}</td>
                        <td>${el.price}</td>
                        </tr>
        
                    `)  
                    
                });
    
                $('#btnShowAllId').hide()
            }
        })
    
    
    })

}



   


