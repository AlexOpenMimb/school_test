<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
         $products = Product::paginate(8);

        return view('home', ['products' => $products]);
    }

    public function show(Product $product)
    {

        try {
         
            return json_encode($product);

        } catch (\Exception $e) {

            $message = $e->getMessage();
            return redirect()->back()->with('error','Error: '. $message,); 
        }
    }

    public function get()
    {
        
        try {
            $products = Product::all();
         
            return json_encode($products);

        } catch (\Exception $e) {

            $message = $e->getMessage();
            return redirect()->back()->with('error','Error: '. $message,); 
        }
    }

    
}
