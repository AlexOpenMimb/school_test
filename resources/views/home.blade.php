<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
      
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
    <body>
        <div class="flex-center position-ref full-height">

        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <!-- llamamos al logo de Vue -->
                    <img src="" alt="" width="30" height="24">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <router-link exact-active-class="active" to="/" class="nav-link active" aria-current="page">Inicio</router-link>
                    </li>
                               
                </ul>
                <form class="d-flex">
                    <label for=""></label>
                    <input id="searchId" class="form-control me-2" type="search" placeholder="Buscar por Product key" aria-label="Search">
                    <button id="btnId" class="btn btn btn-success" type="submit">Buscar</button>
                    
                </form>
                </div>
            </div>
        </nav>

        </div>
        

        
        <div class="container">
            <div class="d-flex justify-content-between">

                <div class="d-flex">
                    <h1>Produtos</h1> 
                    <div class="m-2">
                        <button id="btnShowAllId" class="btn btn btn-success" type="button">Mostrar Todos Los Productos</button>
                    </div>

                </div>
            </div>
            <div id="msmErroId"></div>
            <table id="tableId" class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Item</th>
                  <th scope="col">Product key</th>
                  <th scope="col">Notes</th>
                  <th scope="col">Qty</th>
                  <th scope="col">Cost</th>
                  <th scope="col">Price</th>
                 
                </tr>
              </thead>
              <tbody id="tbodyId">
             
               @foreach ($products as $product)
                  
              <tr>
                  <th scope="row">{{$product->id}}</th>
                  <td>{{$product->product_key}}</td>
                  <td>{{$product->notes}}</td>
                  <td>{{$product->qty}}</td>
                  <td>{{$product->cost}}</td>
                  <td>{{$product->price}}</td>
                  
              </tr>
              @endforeach
                   
              </tbody>
            </table>
            <div id="linksId">

                {{$products->links()}}
            </div>
          </div>






         
          <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
          <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="{{asset('js/ajax/index.js')}}"></script>
    </body>
</html>
